<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimKerjaMembership extends Model
{
    const ACTIVE = 'active';
    const INACTIVE = 'inactive';

    const KETUA = 'ketua';
    const ANGGOTA = 'anggota';

    protected $table = 'tim_kerja_membership';
    public $timestamps = false;

    protected $fillable = [
        'id_tim_kerja_membership',
        'nip',
        'id_tim_kerja',
        'type',
        'role',
        'status',
        'activated_at',
        'deactivated_at',
        'deleted_at'
    ];
}
