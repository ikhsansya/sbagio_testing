<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\SbagioEvent
 *
 * @property int $id
 * @property string $hash
 * @property string $event_type
 * @property int $priority
 * @property string $item_type
 * @property array $item_data
 * @property string $created_at
 * @property string $done_at
 */
class SbagioEvent extends Model
{

    protected $table = 'sbagio_events';

    public $timestamps = false;

    protected $fillable = [
        'hash',
        'event_type',
        'priority',
        'item_type',
        'item_data'
    ];
}
