<?php

namespace LaravelSbagio\Consumers;

use App\Models\TimKerjaMembership as TimKerjaMembershipModel;

class TimKerjaMembership implements \Sbagio\Interfaces\Consumer\TimKerjaMembershipConsumerInterface
{

    public function onTimKerjaMembershipAdded(\Sbagio\Entities\TimKerjaMembership $timKerjaMembership)
    {
        /** @var TimKerjaMembershipModel $member */
        $member = TimKerjaMembershipModel::where('id_tim_kerja_membership', $timKerjaMembership->id)->first();

        if (!empty($member)) {
            return false;
        }

        /** @var TimKerjaMembershipModel $newMember */
        $newMember = new TimKerjaMembershipModel();
        $newMember->id_tim_kerja_membership = $timKerjaMembership->id;
        $newMember->nip = $timKerjaMembership->nip;
        $newMember->id_tim_kerja = $timKerjaMembership->idTimKerja;
        $newMember->type = $timKerjaMembership->type;
        $newMember->role = $timKerjaMembership->role;
        $newMember->status = $timKerjaMembership->status;
        $newMember->activated_at = $timKerjaMembership->activatedAt;
        $newMember->deactivated_at = $timKerjaMembership->deactivatedAt;
        $newMember->deleted_at = $timKerjaMembership->deletedAt;

        if ($newMember->save()) {
            event('tim-kerja-membership.added', [$newMember]);
            return true;
        }

        return false;
    }

    public function onTimKerjaMembershipDeactivated(\Sbagio\Entities\TimKerjaMembership $timKerjaMembership)
    {
        /** @var TimKerjaMembershipModel $member */
        $member = TimKerjaMembershipModel::where('id_tim_kerja_membership', $timKerjaMembership->id)->first();

        if (empty($member)) {
            return false;
        }

        $member->nip = $timKerjaMembership->nip;
        $member->id_tim_kerja = $timKerjaMembership->idTimKerja;
        $member->type = $timKerjaMembership->type;
        $member->role = $timKerjaMembership->role;
        $member->status = $timKerjaMembership->status;
        $member->activated_at = $timKerjaMembership->activatedAt;
        $member->deactivated_at = $timKerjaMembership->deactivatedAt;
        $member->deleted_at = $timKerjaMembership->deletedAt;

        if ($member->save()) {
            event('tim-kerja-membership.deactivated', [$member]);
            return true;
        }

        return false;
    }

    public function onTimKerjaMembershipUpdated(\Sbagio\Entities\TimKerjaMembership $timKerjaMembership)
    {
        /** @var TimKerjaMembershipModel $member */
        $member = TimKerjaMembershipModel::where('id_tim_kerja_membership', $timKerjaMembership->id)->first();

        if (empty($member)) {
            return false;
        }

        $member->nip = $timKerjaMembership->nip;
        $member->id_tim_kerja = $timKerjaMembership->idTimKerja;
        $member->type = $timKerjaMembership->type;
        $member->role = $timKerjaMembership->role;
        $member->status = $timKerjaMembership->status;
        $member->activated_at = $timKerjaMembership->activatedAt;
        $member->deactivated_at = $timKerjaMembership->deactivatedAt;
        $member->deleted_at = $timKerjaMembership->deletedAt;

        if ($member->save()) {
            event('tim-kerja-membership.updated', [$member]);
            return true;
        }

        return false;
    }
}
