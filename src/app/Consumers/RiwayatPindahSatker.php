<?php

namespace LaravelSbagio\Consumers;

use App\Models\Organization;
use App\Models\Pegawai;

use Sbagio\Entities\RemovedRiwayatPindahSatker;
use Sbagio\Entities\RiwayatPindahSatker as RiwayatPindahSatkerEntity;
use Sbagio\Interfaces\Consumer\RiwayatPindahSatkerConsumerInterface;

class RiwayatPindahSatker implements RiwayatPindahSatkerConsumerInterface
{
    const RIWAYAT_LAMA = 0;
    const RIWAYAT_BARU = 1;

    public function onRiwayatPindahSatkerCreated(RiwayatPindahSatkerEntity $riwayatPindahSatker)
    {
        if ($riwayatPindahSatker->statusRiwayat == self::RIWAYAT_LAMA) {
            return false;
        }

        /** @var Pegawai $model */
        $model = Pegawai::where('nip', $riwayatPindahSatker->nip)->first();
        /** @var Organization $org */
        $org = Organization::where('kode_simpatik', $riwayatPindahSatker->kodeUnitKerjaTujuan)->first();

        if ($model) {
            $model->organisasi = json_encode([
                'path' => $org->path ?? '-',
                'nama' => $org->nama_organisasi ?? '-',
                'kota' => $org->nama_kota ?? '-'
            ]);
            if ($model->save()) {
                event('pindah-satker.created', [$model]);
                return true;
            }
        }
        return false;
    }

    public function onRiwayatPindahSatkerUpdated(RiwayatPindahSatkerEntity $riwayatPindahSatker)
    {
        if ($riwayatPindahSatker->statusRiwayat == self::RIWAYAT_LAMA) {
            return false;
        }

        /** @var Pegawai $model */
        $model = Pegawai::where('nip', $riwayatPindahSatker->nip)->first();

        /** @var Organization $org */
        $org = Organization::where('kode_simpatik', $riwayatPindahSatker->kodeUnitKerjaTujuan)->first();

        if ($model) {
            $model->organisasi = json_encode([
                'path' => $org->path ?? '-',
                'nama' => $org->nama_organisasi ?? '-',
                'kota' => $org->nama_kota ?? '-'
            ]);
            if ($model->save()) {
                event('pindah-satker.updated', [$model]);
                return true;
            }
        }
        return false;
    }

    public function onRiwayatPindahSatkerRemoved(RemovedRiwayatPindahSatker $removedRiwayatPindahSatker)
    {
        $model = Pegawai::where('nip', $removedRiwayatPindahSatker->nip)->first();
        if ($model) {

            if ($model->save()) {
                event('pindah-satker.removed', [$model]);
                return true;
            }
        }
        return false;
    }
}
