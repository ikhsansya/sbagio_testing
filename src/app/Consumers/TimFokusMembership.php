<?php

namespace LaravelSbagio\Consumers;

use App\Models\TimFokusMembership as TimFokusMembershipModel;

class TimFokusMembership implements \Sbagio\Interfaces\Consumer\TimFokusMembershipConsumerInterface
{

    public function onTimFokusMembershipAdded(\Sbagio\Entities\TimFokusMembership $timFokusMembership)
    {
        /** @var TimFokusMembershipModel $member */
        $member = TimFokusMembershipModel::where('id_tim_fokus_membership', $timFokusMembership->id)->first();

        if (!empty($member)) {
            return false;
        }

        /** @var TimFokusMembershipModel $newMember */
        $newMember = new TimFokusMembershipModel();
        $newMember->id_tim_fokus_membership = $timFokusMembership->id;
        $newMember->nip = $timFokusMembership->nip;
        $newMember->id_tim_fokus = $timFokusMembership->idTimFokus;
        $newMember->type = $timFokusMembership->type;
        $newMember->role = $timFokusMembership->role;
        $newMember->status = $timFokusMembership->status;
        $newMember->activated_at = $timFokusMembership->activatedAt;
        $newMember->deactivated_at = $timFokusMembership->deactivatedAt;
        $newMember->deleted_at = $timFokusMembership->deletedAt;

        if ($newMember->save()) {
            event('tim-fokus-membership.added', [$newMember]);
            return true;
        }

        return false;
    }

    public function onTimFokusMembershipDeactivated(\Sbagio\Entities\TimFokusMembership $timFokusMembership)
    {
        /** @var TimFokusMembershipModel $member */
        $member = TimFokusMembershipModel::where('id_tim_fokus_membership', $timFokusMembership->id)->first();

        if (empty($member)) {
            return false;
        }

        $member->nip = $timFokusMembership->nip;
        $member->id_tim_fokus = $timFokusMembership->idTimFokus;
        $member->type = $timFokusMembership->type;
        $member->role = $timFokusMembership->role;
        $member->status = $timFokusMembership->status;
        $member->activated_at = $timFokusMembership->activatedAt;
        $member->deactivated_at = $timFokusMembership->deactivatedAt;
        $member->deleted_at = $timFokusMembership->deletedAt;

        if ($member->save()) {
            event('tim-fokus-membership.deactivated', [$member]);
            return true;
        }

        return false;
    }

    public function onTimFokusMembershipUpdated(\Sbagio\Entities\TimFokusMembership $timFokusMembership)
    {
        /** @var TimFokusMembershipModel $member */
        $member = TimFokusMembershipModel::where('id_tim_fokus_membership', $timFokusMembership->id)->first();

        if (empty($member)) {
            return false;
        }

        $member->nip = $timFokusMembership->nip;
        $member->id_tim_fokus = $timFokusMembership->idTimFokus;
        $member->type = $timFokusMembership->type;
        $member->role = $timFokusMembership->role;
        $member->status = $timFokusMembership->status;
        $member->activated_at = $timFokusMembership->activatedAt;
        $member->deactivated_at = $timFokusMembership->deactivatedAt;
        $member->deleted_at = $timFokusMembership->deletedAt;

        if ($member->save()) {
            event('tim-fokus-membership.updated', [$member]);
            return true;
        }

        return false;
    }
}
