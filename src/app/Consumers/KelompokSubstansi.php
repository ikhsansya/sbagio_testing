<?php


namespace LaravelSbagio\Consumers;

use Sbagio\Entities\KelompokSubstansi as KelompokSubstansiEntity;
use Sbagio\Interfaces\Consumer\KelompokSubstansiConsumerInterface;

use App\Models\KelompokSubstansi as KelompokSubstansiModel;
use App\Models\Organization;

class KelompokSubstansi implements KelompokSubstansiConsumerInterface
{
    public function findKelompokSubstansi($kode = false)
    {
        $model = KelompokSubstansiModel::where(['kode' => $kode])->first();
        return $model;
    }

    public function findOrganization($kode = false)
    {
        $model = Organization::where(['kode_simpatik' => $kode])->first();
        return $model;
    }

    /**
     * @param KelompokSubstansiEntity $kelompokSubstansi
     * @return bool
     * @throws \Exception
     */
    public function onKelompokSubstansiCreated(KelompokSubstansiEntity $kelompokSubstansi)
    {
        $model = $this->findKelompokSubstansi($kelompokSubstansi->kode);

        if (empty($model)) {

            $parent = ($kelompokSubstansi->parent != '-')
                ? $kelompokSubstansi->parent
                : 0;

            $parentSimpatik = ($kelompokSubstansi->parentKodeUnitKerja != '-')
                ? $kelompokSubstansi->parentKodeUnitKerja
                : 0;

            $model = new KelompokSubstansiModel();
            $model->kode = $kelompokSubstansi->kode;
            $model->parent = $parent;
            $model->nama = strtoupper($kelompokSubstansi->nama);
            $model->deskripsi = $kelompokSubstansi->deskripsi;
            $model->active = $kelompokSubstansi->active;
            $model->parent_kode_unit_kerja = $parentSimpatik;
            $model->path = $kelompokSubstansi->path;
            if ($model->save()) {
                $this->createdOrganization($kelompokSubstansi);
                event('kelompok-substansi.created', [$model]);
                return true;
            }
        }

        return false;
    }

    /**
     * @param KelompokSubstansiEntity $kelompokSubstansi
     * @return bool
     * @throws \Exception
     */
    public function onKelompokSubstansiNameChanged(KelompokSubstansiEntity $kelompokSubstansi)
    {
        $model = $this->findKelompokSubstansi($kelompokSubstansi->kode);

        if (!empty($model)) {
            $model->nama = strtoupper($kelompokSubstansi->nama);
            $model->deskripsi = $kelompokSubstansi->deskripsi;
            if ($model->save()) {
                $this->updatedOrganization($kelompokSubstansi);
                event('kelompok-substansi.updated', [$model]);
                return true;
            }
        }

        return false;
    }

    /**
     * @param KelompokSubstansiEntity $kelompokSubstansi
     * @return bool
     * @throws \Exception
     */
    public function onKelompokSubstansiActivated(KelompokSubstansiEntity $kelompokSubstansi)
    {
        $model = $this->findKelompokSubstansi($kelompokSubstansi->kode);

        if (!empty($model)) {
            $model->active = true;
            if ($model->save()) {
                $this->activatedOrganization($kelompokSubstansi);
                event('kelompok-substansi.activated', [$model]);
                return true;
            }
        }

        return false;
    }

    /**
     * @param KelompokSubstansiEntity $kelompokSubstansi
     * @return bool
     * @throws \Exception
     */
    public function onKelompokSubstansiDeactivated(KelompokSubstansiEntity $kelompokSubstansi)
    {
        $model = $this->findKelompokSubstansi($kelompokSubstansi->kode);

        if (!empty($model)) {
            $model->active = false;
            if ($model->save()) {
                $this->deactivatedOrganization($kelompokSubstansi);
                event('kelompok-substansi.deactivated', [$model]);
                return true;
            }
        }

        return false;
    }

    private function createdOrganization(KelompokSubstansiEntity $kelompokSubstansi)
    {
        $model = $this->findOrganization($kelompokSubstansi->kode);

        if (!$model) {
            /** @var Organization $model */
            $model = new Organization();
            $model->nama_organisasi = strtoupper($kelompokSubstansi->nama);
            $model->nama_kota = 'Jakarta';
            $model->nama_jabatan = strtoupper($kelompokSubstansi->nama);
            $model->active = true;
            if ($model->save()) {
                if ($kelompokSubstansi->parent != 0) {
                    $model->setKode($kelompokSubstansi->kode, $kelompokSubstansi->parent);
                } else {
                    $eselon2 = $this->getEselonFromPathKelompokSubstansi($kelompokSubstansi, 2);
                    $model->setKode($kelompokSubstansi->kode, $eselon2);
                }
                $model->save();
            }
        }
    }

    private function updatedOrganization(KelompokSubstansiEntity $kelompokSubstansi)
    {
        $model = $this->findOrganization($kelompokSubstansi->kode);

        if ($model) {
            $model->nama_organisasi = strtoupper($kelompokSubstansi->nama);
            $model->nama_jabatan = strtoupper($kelompokSubstansi->nama);
            $model->save();
        }
    }

    private function activatedOrganization(KelompokSubstansiEntity $kelompokSubstansi)
    {
        $model = $this->findOrganization($kelompokSubstansi->kode);

        if ($model) {
            $model->active = true;
            $model->save();
        }
    }

    private function deactivatedOrganization(KelompokSubstansiEntity $kelompokSubstansi)
    {
        $model = $this->findOrganization($kelompokSubstansi->kode);

        if ($model) {
            $model->active = false;
            $model->save();
        }
    }

    private function getEselonFromPathKelompokSubstansi(KelompokSubstansiEntity $kelompokSubstansi, $eselon = 0)
    {
        $list = explode('-', $kelompokSubstansi->path);
        $result = 0;

        switch ($eselon) {
            case '0':
                $result = $list[0] ?? 0;
                break;
            case '1':
                $result = $list[1] ?? 0;
                break;
            case '2':
                $result = $list[2] ?? 0;
                break;
        }

        return $result;
    }

}
