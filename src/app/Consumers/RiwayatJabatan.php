<?php

namespace LaravelSbagio\Consumers;


use App\Models\Organization;
use Sbagio\Entities\RemovedRiwayatJabatan;
use Sbagio\Entities\RiwayatJabatan as RiwayatJabatanEntity;
use Sbagio\Interfaces\Consumer\RiwayatJabatanConsumerInterface;

use App\Models\Pegawai;


class RiwayatJabatan implements RiwayatJabatanConsumerInterface
{

    const STRUKTURAL = 'S';
    const JFT = 'F';
    const JFU = 'U';

    public function onRiwayatJabatanCreated(RiwayatJabatanEntity $riwayatJabatan)
    {
        /** @var Pegawai $model */
        $model = Pegawai::where('nip', $riwayatJabatan->nip)->first();
        /** @var Organization $org */
        $org = Organization::where('kode_simpatik', $riwayatJabatan->kodeJabatan)->first();
        /** @var Organization $orgSatker */
        $orgSatker = Organization::where('kode_simpatik', $riwayatJabatan->kodeUnitKerja)->first();

        if ($model) {
            if ($riwayatJabatan->kategoriJabatan == self::STRUKTURAL) {

                if (!$org) {
                    return false;
                }

                if (empty($org->nama_jabatan)) {
                    $org->nama_jabatan = $riwayatJabatan->jabatan;
                    $org->save();
                    event('organization.updated', [$org]);
                }

                $model->organisasi = json_encode([
                    'path'=>$org->path ?? '-',
                    'nama'=>$org->nama_organisasi ?? '-',
                    'kota'=>$org->nama_kota ?? '-'
                ]);

                $model->jabatan_struktural_organisasi = json_encode([
                    'path' => $org->path ?? '-',
                    'nama' => $org->nama_organisasi ?? '-',
                    'kota' => $org->nama_kota ?? '-',
                    'jabatan' => $org->nama_jabatan
                ]);

                $model->nama_jabatan_fungsional_umum = null;
                $model->nama_jabatan_fungsional_tertentu = null;

            } else if ($riwayatJabatan->kategoriJabatan == self::JFT) {
                if ($orgSatker) {
                    $model->organisasi = json_encode([
                        'path'=>$orgSatker->path ?? '-',
                        'nama'=>$orgSatker->nama_organisasi ?? '-',
                        'kota'=>$orgSatker->nama_kota ?? '-'
                    ]);
                }
                $model->jabatan_struktural_organisasi = null;
                $model->nama_jabatan_fungsional_umum = null;
                $model->nama_jabatan_fungsional_tertentu = $riwayatJabatan->jabatan;

            } else {
                if ($orgSatker) {
                    $model->organisasi = json_encode([
                        'path'=>$orgSatker->path ?? '-',
                        'nama'=>$orgSatker->nama_organisasi ?? '-',
                        'kota'=>$orgSatker->nama_kota ?? '-'
                    ]);
                }
                $model->jabatan_struktural_organisasi = null;
                $model->nama_jabatan_fungsional_umum = $riwayatJabatan->jabatan;
                $model->nama_jabatan_fungsional_tertentu = null;
            }

            if ($model->save()) {
                event('jabatan.created', [$model]);
                return true;
            }
        }
        return false;
    }

    public function onRiwayatJabatanUpdated(RiwayatJabatanEntity $riwayatJabatan)
    {
        /** @var Pegawai $model */
        $model = Pegawai::where('nip', $riwayatJabatan->nip)->first();
        /** @var Organization $org */
        $org = Organization::where('kode_simpatik', $riwayatJabatan->kodeJabatan)->first();
        /** @var Organization $orgSatker */
        $orgSatker = Organization::where('kode_simpatik', $riwayatJabatan->kodeUnitKerja)->first();

        if ($model) {
            if ($riwayatJabatan->kategoriJabatan == self::STRUKTURAL) {

                if (!$org) {
                    return false;
                }

                if (empty($org->nama_jabatan)) {
                    $org->nama_jabatan = $riwayatJabatan->jabatan;
                    $org->save();
                    event('organization.updated', [$org]);
                }

                $model->organisasi = json_encode([
                    'path'=>$org->path ?? '-',
                    'nama'=>$org->nama_organisasi ?? '-',
                    'kota'=>$org->nama_kota ?? '-'
                ]);

                $model->jabatan_struktural_organisasi = json_encode([
                    'path' => $org->path ?? '-',
                    'nama' => $org->nama_organisasi ?? '-',
                    'kota' => $org->nama_kota ?? '-',
                    'jabatan' => $org->nama_jabatan
                ]);

                $model->nama_jabatan_fungsional_umum = null;
                $model->nama_jabatan_fungsional_tertentu = null;

            } else if ($riwayatJabatan->kategoriJabatan == self::JFT) {
                if ($orgSatker) {
                    $model->organisasi = json_encode([
                        'path'=>$orgSatker->path ?? '-',
                        'nama'=>$orgSatker->nama_organisasi ?? '-',
                        'kota'=>$orgSatker->nama_kota ?? '-'
                    ]);
                }
                $model->jabatan_struktural_organisasi = null;
                $model->nama_jabatan_fungsional_umum = null;
                $model->nama_jabatan_fungsional_tertentu = $riwayatJabatan->jabatan;

            } else {
                if ($orgSatker) {
                    $model->organisasi = json_encode([
                        'path'=>$orgSatker->path ?? '-',
                        'nama'=>$orgSatker->nama_organisasi ?? '-',
                        'kota'=>$orgSatker->nama_kota ?? '-'
                    ]);
                }
                $model->jabatan_struktural_organisasi = null;
                $model->nama_jabatan_fungsional_umum = $riwayatJabatan->jabatan;
                $model->nama_jabatan_fungsional_tertentu = null;
            }

            if ($model->save()) {
                event('jabatan.updated', [$model]);
                return true;
            }
        }
        return false;
    }

    public function onRiwayatJabatanRemoved(RemovedRiwayatJabatan $removedRiwayatJabatan)
    {
        $model = Pegawai::where('nip', $removedRiwayatJabatan->nip)->first();
        if ($model) {
            if ($removedRiwayatJabatan->kategoriJabatan == self::STRUKTURAL) {
                $model->jabatan_struktural_organisasi = null;
            } else if ($removedRiwayatJabatan->kategoriJabatan == self::JFT) {
                $model->nama_jabatan_fungsional_tertentu = null;
            } else {
                $model->nama_jabatan_fungsional_umum = null;
            }

            if ($model->save()) {
                event('jabatan.removed', [$model]);
                return true;
            }
        }
        return false;
    }

}
