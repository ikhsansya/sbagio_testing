<?php

namespace LaravelSbagio\Consumers;


use Sbagio\Entities\Organisasi as OrganisasiEntity;
use Sbagio\Interfaces\Consumer\OrganisasiConsumerInterface;

use App\Models\Organization;

class Organisasi implements OrganisasiConsumerInterface
{
    public function findOrganization($kode = false)
    {
        $model = Organization::where(['kode_simpatik' => $kode])->first();
        return $model;
    }

    /**
     * @param OrganisasiEntity $organisasi
     * @return bool
     * @throws \Exception
     */
    public function onOrganizationCreated(OrganisasiEntity $organisasi)
    {
        $model = $this->findOrganization($organisasi->kode);
        if (!$model) {
            $model = new Organization();
            $model->nama_organisasi = strtoupper($organisasi->unitKerja);
            $model->nama_kota = 'Jakarta';
            $model->nama_jabatan = null;
            $model->active = true;
            if ($model->save()) {
                $model->setKode($organisasi->kode, $organisasi->idParent);
                if ($model->save()) {
                    event('organization.created', [$model]);
                    return true;
                }
            }
        }
        return false;
    }

    public function onOrganizationNameChanged(OrganisasiEntity $organisasi)
    {
        $model = $this->findOrganization($organisasi->kode);
        if ($model) {
            $model->nama_organisasi = strtoupper($organisasi->unitKerja);
            if ($model->save()) {
                event('organization.created', [$model]);
                return true;
            }
        }
        return false;
    }

    public function onOrganizationActivated(OrganisasiEntity $organisasi)
    {
        $model = $this->findOrganization($organisasi->kode);
        if ($model) {
            $model->active = true;
            if ($model->save()) {
                event('organization.activated', [$model]);
                return true;
            }
        }
        return false;
    }


    public function onOrganizationDeactivated(OrganisasiEntity $organisasi)
    {
        $model = $this->findOrganization($organisasi->kode);
        if ($model) {
            $model->active = false;
            if ($model->save()) {
                event('organization.deactivated', [$model]);
                return true;
            }
        }
        return false;
    }

}
