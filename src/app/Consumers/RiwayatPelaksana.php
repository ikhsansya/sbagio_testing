<?php

namespace LaravelSbagio\Consumers;


use App\Models\Organization;
use App\Models\Pegawai;
use Sbagio\Entities\RemovedRiwayatPelaksana;
use Sbagio\Entities\RiwayatPelaksana as RiwayatPelaksanaEntity;
use Sbagio\Interfaces\Consumer\RiwayatPelaksanaConsumerInterface;

class RiwayatPelaksana implements RiwayatPelaksanaConsumerInterface
{
    const PLT = 'PLT';
    const PLH = 'PLH';

    public function onRiwayatPelaksanaCreated(RiwayatPelaksanaEntity $riwayatPelaksana)
    {
        /** @var Pegawai $model */
        $model = Pegawai::where('nip', $riwayatPelaksana->nip)->first();
        if (!$model) {
            return false;
        }
        $orgAssigned = Organization::where('kode_simpatik', $riwayatPelaksana->kodeUnitKerjaJabatanStruktural)
            ->first();

        if (!$orgAssigned) {
            return false;
        }

        if ($riwayatPelaksana->tipePelaksana == self::PLT) {
            $model->plt_jabatan_struktural_organisasi = json_encode([
                'path' => $orgAssigned->path ?? '-',
                'nama' => $orgAssigned->nama_organisasi ?? '-',
                'kota' => $orgAssigned->nama_kota ?? '-',
                'jabatan' => $orgAssigned->nama_jabatan,
                'active' => $riwayatPelaksana->isActive,
                'tanggal_mulai' => $riwayatPelaksana->tanggalMulai,
                'tanggal_selesai' => $riwayatPelaksana->tanggalSelesai
            ]);
        } else {
            $model->plh_jabatan_struktural_organisasi = json_encode([
                'path' => $orgAssigned->path ?? '-',
                'nama' => $orgAssigned->nama_organisasi ?? '-',
                'kota' => $orgAssigned->nama_kota ?? '-',
                'jabatan' => $orgAssigned->nama_jabatan,
                'active' => $riwayatPelaksana->isActive,
                'tanggal_mulai' => $riwayatPelaksana->tanggalMulai,
                'tanggal_selesai' => $riwayatPelaksana->tanggalSelesai
            ]);
        }

        $model->save();

        event('pelaksana.created', [$model]);

        return true;
    }

    public function onRiwayatPelaksanaUpdated(RiwayatPelaksanaEntity $riwayatPelaksana)
    {
        /** @var Pegawai $model */
        $model = Pegawai::where('nip', $riwayatPelaksana->nip)->first();
        if (!$model) {
            return false;
        }
        $orgAssigned = Organization::where('kode_simpatik', $riwayatPelaksana->kodeUnitKerjaJabatanStruktural)
            ->first();

        if (!$orgAssigned) {
            return false;
        }

        if ($riwayatPelaksana->tipePelaksana == self::PLT) {
            if ($riwayatPelaksana->isActive == '-') {
                $model->plt_jabatan_struktural_organisasi = null;
            } else {
                $model->plt_jabatan_struktural_organisasi = json_encode([
                    'path' => $orgAssigned->path ?? '-',
                    'nama' => $orgAssigned->nama_organisasi ?? '-',
                    'kota' => $orgAssigned->nama_kota ?? '-',
                    'jabatan' => $orgAssigned->nama_jabatan,
                    'active' => $riwayatPelaksana->isActive,
                    'tanggal_mulai' => $riwayatPelaksana->tanggalMulai,
                    'tanggal_selesai' => $riwayatPelaksana->tanggalSelesai
                ]);
            }
        } else {
            if ($riwayatPelaksana->isActive == '-') {
                $model->plh_jabatan_struktural_organisasi = null;
            } else {
                $model->plh_jabatan_struktural_organisasi = json_encode([
                    'path' => $orgAssigned->path ?? '-',
                    'nama' => $orgAssigned->nama_organisasi ?? '-',
                    'kota' => $orgAssigned->nama_kota ?? '-',
                    'jabatan' => $orgAssigned->nama_jabatan,
                    'active' => $riwayatPelaksana->isActive,
                    'tanggal_mulai' => $riwayatPelaksana->tanggalMulai,
                    'tanggal_selesai' => $riwayatPelaksana->tanggalSelesai
                ]);
            }
        }

        $model->save();

        event('pelaksana.updated', [$model]);

        return true;
    }

    public function onRiwayatPelaksanaRemoved(RemovedRiwayatPelaksana $removedRiwayatPelaksana)
    {
        /** @var Pegawai $model */
        $model = Pegawai::where('nip', $removedRiwayatPelaksana->nip)->first();
        if (!$model) {
            return false;
        }
        $orgAssigned = Organization::where('kode_simpatik', $removedRiwayatPelaksana->kodeUnitKerjaJabatanStruktural)
            ->first();

        if (!$orgAssigned) {
            return false;
        }

        if ($removedRiwayatPelaksana->tipePelaksana == self::PLT) {
            $model->plt_jabatan_struktural_organisasi = null;
        } else {
            $model->plh_jabatan_struktural_organisasi = null;
        }

        $model->save();

        event('pelaksana.removed', [$model]);

        return true;
    }

}
