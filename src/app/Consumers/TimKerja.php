<?php

namespace LaravelSbagio\Consumers;

use App\Models\TimKerja as TimKerjaModel;

class TimKerja implements \Sbagio\Interfaces\Consumer\TimKerjaConsumerInterface
{

    public function onTimKerjaCreated(\Sbagio\Entities\TimKerja $timKerja)
    {
        /** @var TimKerjaModel $tim */
        $tim = TimKerjaModel::where('id_tim_kerja', $timKerja->id)->first();

        if (!empty($tim)) {
            return false;
        }

        /** @var TimKerjaModel $newTim */
        $newTim = new TimKerjaModel();
        $newTim->id_tim_kerja = $timKerja->id;
        $newTim->kode_satker = $timKerja->kodeSatker;
        $newTim->name = $timKerja->name;
        $newTim->status = $timKerja->status;
        $newTim->activated_at = $timKerja->activatedAt;
        $newTim->deactivated_at = $timKerja->deactivatedAt;
        $newTim->rejected_at = $timKerja->rejectedAt;
        $newTim->rejected_reason = $timKerja->rejectedReason;

        if ($newTim->save()) {
            event('tim-kerja.created', [$newTim]);
            return true;
        }

        return false;
    }

    public function onTimKerjaUpdated(\Sbagio\Entities\TimKerja $timKerja)
    {
        /** @var TimKerjaModel $tim */
        $tim = TimKerjaModel::where('id_tim_kerja', $timKerja->id)->first();

        if (empty($tim)) {
            return false;
        }

        $tim->kode_satker = $timKerja->kodeSatker;
        $tim->name = $timKerja->name;
        $tim->status = $timKerja->status;
        $tim->activated_at = $timKerja->activatedAt;
        $tim->deactivated_at = $timKerja->deactivatedAt;
        $tim->rejected_at = $timKerja->rejectedAt;
        $tim->rejected_reason = $timKerja->rejectedReason;

        if ($tim->save()) {
            event('tim-kerja.updated', [$tim]);
            return true;
        }

        return false;
    }

    public function onTimKerjaActivated(\Sbagio\Entities\TimKerja $timKerja)
    {
        /** @var TimKerjaModel $tim */
        $tim = TimKerjaModel::where('id_tim_kerja', $timKerja->id)->first();

        if (empty($tim)) {
            return false;
        }

        $tim->kode_satker = $timKerja->kodeSatker;
        $tim->name = $timKerja->name;
        $tim->status = $timKerja->status;
        $tim->activated_at = $timKerja->activatedAt;
        $tim->deactivated_at = $timKerja->deactivatedAt;
        $tim->rejected_at = $timKerja->rejectedAt;
        $tim->rejected_reason = $timKerja->rejectedReason;

        if ($tim->save()) {
            event('tim-kerja.activated', [$tim]);
            return true;
        }

        return false;
    }

    public function onTimKerjaDeactivated(\Sbagio\Entities\TimKerja $timKerja)
    {
        /** @var TimKerjaModel $tim */
        $tim = TimKerjaModel::where('id_tim_kerja', $timKerja->id)->first();

        if (empty($tim)) {
            return false;
        }

        $tim->kode_satker = $timKerja->kodeSatker;
        $tim->name = $timKerja->name;
        $tim->status = $timKerja->status;
        $tim->activated_at = $timKerja->activatedAt;
        $tim->deactivated_at = $timKerja->deactivatedAt;
        $tim->rejected_at = $timKerja->rejectedAt;
        $tim->rejected_reason = $timKerja->rejectedReason;

        if ($tim->save()) {
            event('tim-kerja.deactivated', [$tim]);
            return true;
        }

        return false;
    }
}
