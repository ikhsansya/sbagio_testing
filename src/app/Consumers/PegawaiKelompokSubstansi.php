<?php


namespace LaravelSbagio\Consumers;

use Sbagio\Entities\PegawaiKelompokSubstansi as PegawaiKelompokSubstansiEntity;
use Sbagio\Entities\RemovedPegawaiKelompokSubstansi;
use Sbagio\Interfaces\Consumer\PegawaiKelompokSubstansiConsumerInterface;

use App\Models\KelompokSubstansi;
use App\Models\Pegawai;
use App\Models\Organization;

class PegawaiKelompokSubstansi implements PegawaiKelompokSubstansiConsumerInterface
{
    public function findKelompokSubstansi($kode = false)
    {
        /** @var KelompokSubstansi $model */
        $model = KelompokSubstansi::where(['kode' => $kode])->first();

        return $model;
    }

    public function findPegawai($nip = false)
    {
        /** @var Pegawai $model */
        $model = Pegawai::where('nip', $nip)->first();

        return $model;
    }

    public function findOrganization($kode = false)
    {
        $model = Organization::where(['kode_simpatik' => $kode])->first();
        return $model;
    }

    public function onPegawaiAddedToKelompokSubstansi(PegawaiKelompokSubstansiEntity $pegawaiKelompokSubstansi)
    {
        $pegawai = $this->findPegawai($pegawaiKelompokSubstansi->nip);
        $organisasi = $this->findOrganization($pegawaiKelompokSubstansi->kodeKelompokSubstansi);
        $kelompokSubstansi = $this->findKelompokSubstansi($pegawaiKelompokSubstansi->kodeKelompokSubstansi);

        if (empty($pegawai) ||
            empty($kelompokSubstansi) ||
            empty($organisasi)) {
            return false;
        }

        $pegawai->jabatan_kelompok_substansi = json_encode([
            'path' => $organisasi->path ?? '-',
            'nama' => $organisasi->nama_organisasi ?? '-',
            'kota' => $organisasi->nama_kota ?? '-',
            'jabatan' => $organisasi->nama_jabatan,
            'active' => $organisasi->active,
            'role' => $pegawaiKelompokSubstansi->role,
            'tanggal_mulai' => $pegawaiKelompokSubstansi->start,
            'tanggal_selesai' => $pegawaiKelompokSubstansi->end
        ]);

        if ($pegawai->save()) {
            event('pegawai-kelompok-substansi.created', [$pegawai]);
            return true;
        }
    }

    public function onPegawaiKelompokSubstansiRoleChanged(PegawaiKelompokSubstansiEntity $pegawaiKelompokSubstansi)
    {
        $pegawai = $this->findPegawai($pegawaiKelompokSubstansi->nip);
        $organisasi = $this->findOrganization($pegawaiKelompokSubstansi->kodeKelompokSubstansi);
        $kelompokSubstansi = $this->findKelompokSubstansi($pegawaiKelompokSubstansi->kodeKelompokSubstansi);

        if (empty($pegawai) ||
            empty($kelompokSubstansi) ||
            empty($organisasi)) {
            return false;
        }

        $pegawai->jabatan_kelompok_substansi = json_encode([
            'path' => $organisasi->path ?? '-',
            'nama' => $organisasi->nama_organisasi ?? '-',
            'kota' => $organisasi->nama_kota ?? '-',
            'jabatan' => $organisasi->nama_jabatan,
            'active' => $organisasi->active,
            'role' => $pegawaiKelompokSubstansi->role,
            'tanggal_mulai' => $pegawaiKelompokSubstansi->start,
            'tanggal_selesai' => $pegawaiKelompokSubstansi->end
        ]);

        if ($pegawai->save()) {
            event('pegawai-kelompok-substansi.updated', [$pegawai]);
            return true;
        }
    }

    public function onPegawaiRemovedFromKelompokSubstansi(RemovedPegawaiKelompokSubstansi $removedPegawaiKelompokSubstansi)
    {
        $pegawai = $this->findPegawai($removedPegawaiKelompokSubstansi->nip);
        $organisasi = $this->findOrganization($removedPegawaiKelompokSubstansi->kodeKelompokSubstansi);
        $kelompokSubstansi = $this->findKelompokSubstansi($removedPegawaiKelompokSubstansi->kodeKelompokSubstansi);

        if (empty($pegawai) ||
            empty($kelompokSubstansi) ||
            empty($organisasi)) {
            return false;
        }

        $pegawai->jabatan_kelompok_substansi = null;

        if ($pegawai->save()) {
            event('pegawai-kelompok-substansi.removed', [$pegawai]);
            return true;
        }
    }
}
