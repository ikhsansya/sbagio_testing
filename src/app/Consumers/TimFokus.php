<?php

namespace LaravelSbagio\Consumers;

use App\Models\TimFokus as TimFokusModel;

class TimFokus implements \Sbagio\Interfaces\Consumer\TimFokusConsumerInterface
{

    public function onTimFokusCreated(\Sbagio\Entities\TimFokus $timFokus)
    {
        /** @var TimFokusModel $tim */
        $tim = TimFokusModel::where('id_tim_fokus', $timFokus->id)->first();

        if (!empty($tim)) {
            return false;
        }

        /** @var TimFokusModel $newTim */
        $newTim = new TimFokusModel();
        $newTim->id_tim_fokus = $timFokus->id;
        $newTim->id_tim_kerja = $timFokus->idTimKerja;
        $newTim->name = $timFokus->name;
        $newTim->status = $timFokus->status;
        $newTim->activated_at = $timFokus->activatedAt;
        $newTim->deactivated_at = $timFokus->deactivatedAt;

        if ($newTim->save()) {
            event('tim-fokus.created', [$newTim]);
            return true;
        }

        return false;
    }

    public function onTimFokusUpdated(\Sbagio\Entities\TimFokus $timFokus)
    {
        /** @var TimFokusModel $tim */
        $tim = TimFokusModel::where('id_tim_fokus', $timFokus->id)->first();

        if (empty($tim)) {
            return false;
        }

        $tim->id_tim_kerja = $timFokus->idTimKerja;
        $tim->name = $timFokus->name;
        $tim->status = $timFokus->status;
        $tim->activated_at = $timFokus->activatedAt;
        $tim->deactivated_at = $timFokus->deactivatedAt;

        if ($tim->save()) {
            event('tim-fokus.updated', [$tim]);
            return true;
        }

        return false;
    }

    public function onTimFokusActivated(\Sbagio\Entities\TimFokus $timFokus)
    {
        /** @var TimFokusModel $tim */
        $tim = TimFokusModel::where('id_tim_fokus', $timFokus->id)->first();

        if (empty($tim)) {
            return false;
        }

        $tim->id_tim_kerja = $timFokus->idTimKerja;
        $tim->name = $timFokus->name;
        $tim->status = $timFokus->status;
        $tim->activated_at = $timFokus->activatedAt;
        $tim->deactivated_at = $timFokus->deactivatedAt;

        if ($tim->save()) {
            event('tim-fokus.activated', [$tim]);
            return true;
        }

        return false;
    }

    public function onTimFokusDeactivated(\Sbagio\Entities\TimFokus $timFokus)
    {
        /** @var TimFokusModel $tim */
        $tim = TimFokusModel::where('id_tim_fokus', $timFokus->id)->first();

        if (empty($tim)) {
            return false;
        }

        $tim->id_tim_kerja = $timFokus->idTimKerja;
        $tim->name = $timFokus->name;
        $tim->status = $timFokus->status;
        $tim->activated_at = $timFokus->activatedAt;
        $tim->deactivated_at = $timFokus->deactivatedAt;

        if ($tim->save()) {
            event('tim-fokus.deactivated', [$tim]);
            return true;
        }

        return false;
    }
}
