<?php

namespace LaravelSbagio\Consumers;


use App\Models\Organization;
use App\Models\Pegawai as PegawaiModel;
use Carbon\Carbon;
use Sbagio\Entities\Pegawai as PegawaiEntity;
use Sbagio\Entities\PegawaiId;
use Sbagio\Entities\PegawaiShort;
use Sbagio\Entities\PegawaiStatus;
use Sbagio\Interfaces\Consumer\PegawaiConsumerInterface;


class Pegawai implements PegawaiConsumerInterface
{
    const ACTIVE = 1;
    const NON_ACTIVE = 0;

    const STRUKTURAL = 'STRUKTURAL';
    const JFU = 'FUNGSIONAL UMUM';
    const JFT = 'FUNGSIONAL TERTENTU';

    public function onPegawaiCreated(PegawaiEntity $pegawai)
    {
        try {
            // skip exist user
            if ($model = PegawaiModel::where('nip', $pegawai->nip)->first()) {
                return false;
            }

            // skip user non active
            if ($pegawai->username && $pegawai->portalStatusAktif == '0') {
                return false;
            }

            $model = new PegawaiModel();
            $model->nip = $pegawai->nip;
            $model->name = trim($pegawai->nama);
            $model->email_verified_at = Carbon::now()->format('Y-m-d H:i:s');
            $model->pangkat_golongan = $pegawai->pangkatKode != '-'
                ? "$pegawai->pangkat ({$pegawai->pangkatKode})"
                : null;

            /** @var Organization $org */
            $org = Organization::where('kode_simpatik', $pegawai->kodeUnitKerja);
            if(!$org->exists()) {
                return false;
            }
            $org = $org->first();
            $model->organisasi = json_encode([
                'path' => $org->path ?? '-',
                'nama' => $org->nama_organisasi ?? '-',
                'kota' => $org->nama_kota ?? '-'
            ]);

            if ($pegawai->tipePegawai === self::STRUKTURAL) {
                if (empty($org->nama_jabatan)) {
                    $org->nama_jabatan = $pegawai->jabatan;
                    $org->save();
                    event('organization.updated', [$org]);
                }
                $model->jabatan_struktural_organisasi = json_encode([
                    'path' => $org->path ?? '-',
                    'nama' => $org->nama_organisasi ?? '-',
                    'kota' => $org->nama_kota ?? '-',
                    'jabatan' => $org->nama_jabatan
                ]);
            } else if ($pegawai->tipePegawai === self::JFT) {
                $model->nama_jabatan_fungsional_tertentu = $pegawai->jabatan;
            } else if ($pegawai->tipePegawai === self::JFU) {
                $model->nama_jabatan_fungsional_umum = $pegawai->jabatan;
            }

            $model->username = '-';
            $model->email = '-';

            if ($pegawai->username && $pegawai->username != '-') {
                $model->username = $pegawai->username;
                $model->email = $pegawai->username . '@kominfo.go.id';
            }

            $model->active = self::NON_ACTIVE;
            $model->password = '-';

            if ($pegawai->portalStatusAktif == '1') {
                $model->active = self::ACTIVE;
            }

            if ($model->save()) {
                event('pegawai.created', [$model]);
                return true;
            }
        } catch (\Exception $e) {
            
        }

        return false;
    }

    public function onPegawaiUpdated(PegawaiEntity $pegawai)
    {
        try {
            // skip user non active
            if ($pegawai->username && $pegawai->portalStatusAktif == '0') {
                return false;
            }

            $model = PegawaiModel::where('nip', $pegawai->nip);

            if (!$model->exists()) {
                $model = new PegawaiModel();

                /** @var Organization $org */
                $org = Organization::where('kode_simpatik', $pegawai->kodeUnitKerja);
                if(!$org->exists()) {
                    return false;
                }
                $org = $org->first();

                $model->organisasi = json_encode([
                    'path' => $org->path ?? '-',
                    'nama' => $org->nama_organisasi ?? '-',
                    'kota' => $org->nama_kota ?? '-'
                ]);

                if ($pegawai->tipePegawai === self::STRUKTURAL) {
                    if (empty($org->nama_jabatan)) {
                        $org->nama_jabatan = $pegawai->jabatan;
                        $org->save();
                        event('organization.updated', [$org]);
                    }
                    $model->jabatan_struktural_organisasi = json_encode([
                        'path' => $org->path ?? '-',
                        'nama' => $org->nama_organisasi ?? '-',
                        'kota' => $org->nama_kota ?? '-',
                        'jabatan' => $org->nama_jabatan
                    ]);
                } else if ($pegawai->tipePegawai === self::JFT) {
                    $model->nama_jabatan_fungsional_tertentu = $pegawai->jabatan;
                } else if ($pegawai->tipePegawai === self::JFU) {
                    $model->nama_jabatan_fungsional_umum = $pegawai->jabatan;
                }

                $model->email_verified_at = Carbon::now()->format('Y-m-d H:i:s');
                $model->username = '-';
                $model->name = trim($pegawai->nama);
                $model->email = '-';
                $model->pangkat_golongan = $pegawai->pangkatKode != '-'
                    ? "$pegawai->pangkat ({$pegawai->pangkatKode})"
                    : null;

                if ($pegawai->username && $pegawai->username != '-') {
                    $model->username = $pegawai->username;
                    $model->email = $pegawai->username . '@kominfo.go.id';
                }

                $model->active = self::NON_ACTIVE;
                $model->password = '-';

                if ($pegawai->portalStatusAktif == '1') {
                    $model->active = self::ACTIVE;
                }
            }

            $model = $model->first();

            $model->nip = $pegawai->nip;
            $model->name = trim($pegawai->nama);
            $model->pangkat_golongan = $pegawai->pangkatKode != '-'
                ? "$pegawai->pangkat ({$pegawai->pangkatKode})"
                : null;

            if ($model->save()) {
                event('pegawai.updated', [$model]);
                return true;
            }
        } catch (\Exception $e) {
            
        }

        return false;
    }

    public function onPegawaiPhotoUploaded(PegawaiShort $pegawai)
    {
        try {
            /** @var PegawaiModel $model */
            $model = PegawaiModel::where('nip', $pegawai->nip);

            if ($model->exists()) {
                $model = $model->first();

                $model->profile_picture = $pegawai->imagesPhoto;
                $model->save();

                event('pegawai.photo-uploaded', [$model]);
                return true;
            }
        } catch (\Exception $e) {
            
        }

        return false;
    }

    public function onPegawaiPhotoRemoved(PegawaiShort $pegawaiShort)
    {
        try {
            /** @var PegawaiModel $model */
            $model = PegawaiModel::where('nip', $pegawaiShort->nip);

            if ($model->exists()) {
                $model = $model->first();

                $model->profile_picture = null;
                $model->save();

                event('pegawai.photo-removed', [$model]);
                return true;
            }
        } catch (\Exception $e) {
            
        }

        return false;
    }

    public function onUsernameChanged(PegawaiShort $pegawaiShort)
    {
        try {
            $model = PegawaiModel::where('nip', $pegawaiShort->nip);

            if ($model->exists()) {
                $model = $model->first();

                $model->username = $pegawaiShort->username;
                $model->email = $pegawaiShort->username . '@kominfo.go.id';
                $model->active = self::ACTIVE;

                if ($model->save()) {
                    event('pegawai.username-changed', [$model]);
                    return true;
                }
            }
        } catch (\Exception $e) {
            
        }

        return false;
    }

    public function onMutasiJabatanOccured(PegawaiEntity $pegawai)
    {
        try {
            /** @var PegawaiModel $model */
            $model = PegawaiModel::where('nip', $pegawai->nip);

            if ($model->exists()) {
                $model = $model->first();

                /** @var Organization $org */
                $org = Organization::where('kode_simpatik', $pegawai->kodeUnitKerja);
                if(!$org->exists()) {
                    return false;
                }
                $org = $org->first();
                $model->organisasi = json_encode([
                    'path' => $org->path ?? '-',
                    'nama' => $org->nama_organisasi ?? '-',
                    'kota' => $org->nama_kota ?? '-'
                ]);

                if ($model->save()) {
                    event('pegawai.jabatan-updated', [$model]);
                    return true;
                }
            }
        } catch (\Exception $e) {
            
        }

        return false;
    }

    public function onStatusPegawaiChanged(PegawaiStatus $pegawaiStatus)
    {
        try {
            $model = PegawaiModel::where('nip', $pegawaiStatus->nip);

            if ($model->exists()) {
                $model = $model->first();

                $model->active = false;
                if ($pegawaiStatus->portalStatusAktif == '1') {
                    $model->active = true;
                }
                if ($model->save()) {
                    event('pegawai.status-updated', [$model]);
                    return true;
                }
            }
        } catch (\Exception $e) {
            
        }

        return false;
    }

    public function onPasswordChanged(PegawaiId $pegawaiId)
    {
        //NOT IMPLEMENTED
    }

    public function onPegawaiActivated(PegawaiStatus $pegawaiStatus)
    {
        try {
            $model = PegawaiModel::where('nip', $pegawaiStatus->nip);

            if ($model->exists()) {
                $model = $model->first();

                $model->active = true;
                if ($model->save()) {
                    event('pegawai.activated', [$model]);
                    return true;
                }
            }
        } catch (\Exception $e) {
            
        }

        return false;
    }

    public function onPegawaiDeactivated(PegawaiStatus $pegawaiStatus)
    {
        try {
            $model = PegawaiModel::where('nip', $pegawaiStatus->nip);

            if ($model->exists()) {
                $model = $model->first();

                $model->active = false;
                if ($model->save()) {
                    event('pegawai.deactivated', [$model]);
                    return true;
                }
            }
        } catch (\Exception $e) {
            
        }

        return false;
    }

}
