<?php

namespace LaravelSbagio\Commands;

use Illuminate\Console\Command;
use Sbagio\Sbagio;

class LumenPublish extends Command
{
    protected $signature = 'sbagio:lumen-publish';
    protected $description = 'melakukan publish berkas model dan migrations';

    /**
     * @throws \Exception
     */
    public function handle()
    {
        if (!preg_match('/lumen/i', app()->version())) {
            throw new \Exception('Harap eksekusi dari lumen saja :eyeroll:');
        }

        $dir = dirname(__FILE__) . '/../..';
        $copyMap = collect([]);

        //migrations
        $sbagioMigrationFile = glob(database_path('migrations/*create_sbagio_tables*'));
        if (count($sbagioMigrationFile) <= 0) {
            $timestamp = date('Y_m_d_His', time());
            $copyMap[$dir . '/migrations/create_sbagio_tables.php'] = database_path('migrations/' . $timestamp . '_create_sbagio_tables.php');
        }

        $kosubMigrationFile = glob(database_path('migrations/*create_kelompok_substansi_tables*'));
        if (count($kosubMigrationFile) <= 0) {
            $timestamp = date('Y_m_d_His', time() + 2);
            $copyMap[$dir . '/migrations/create_kelompok_substansi_tables.php'] = database_path('migrations/' . $timestamp . '_create_kelompok_substansi_tables.php');
        }

        $timKerjaMigrationFile = glob(database_path('migrations/*create_tim_kerja_tables*'));
        if (count($timKerjaMigrationFile) <= 0) {
            $timestamp = date('Y_m_d_His', time() + 2);
            $copyMap[$dir . '/migrations/create_tim_kerja_tables.php'] = database_path('migrations/' . $timestamp . '_create_tim_kerja_tables.php');
        }

        //models
        if (!file_exists(app_path('Models/Organization.php'))) {
            $copyMap[$dir . '/models/Organization.php.txt'] = app_path('Models/Organization.php');
        }

        if (!file_exists(app_path('Models/Pegawai.php'))) {
            $copyMap[$dir . '/models/Pegawai.php.txt'] = app_path('Models/Pegawai.php');
        }

        if (!file_exists(app_path('Models/SbagioEvent.php'))) {
            $copyMap[$dir . '/models/SbagioEvent.php.txt'] = app_path('Models/SbagioEvent.php');
        }

        if (!file_exists(app_path('Models/KelompokSubstansi.php'))) {
            $copyMap[$dir . '/models/KelompokSubstansi.php.txt'] = app_path('Models/KelompokSubstansi.php');
        }

        if (!file_exists(app_path('Models/Timkerja.php'))) {
            $copyMap[$dir . '/models/Timkerja.php.txt'] = app_path('Models/Timkerja.php');
        }

        if (!file_exists(app_path('Models/TimkerjaMembership.php'))) {
            $copyMap[$dir . '/models/TimkerjaMembership.php.txt'] = app_path('Models/TimkerjaMembership.php');
        }

        if (!file_exists(app_path('Models/TimFokus.php'))) {
            $copyMap[$dir . '/models/TimFokus.php.txt'] = app_path('Models/TimFokus.php');
        }

        if (!file_exists(app_path('Models/TimFokusMembership.php'))) {
            $copyMap[$dir . '/models/TimFokusMembership.php.txt'] = app_path('Models/TimFokusMembership.php');
        }

        if ($copyMap->isNotEmpty()) {
            foreach ($copyMap as $source => $target) {
                copy($source, $target);
            }

            echo "berkas telah selesai dipublish..." . PHP_EOL;

        } else {
            echo "tidak ada berkas yang dipublish, kamu telah memiliki semuanya ;)" . PHP_EOL;
        }
    }
}
