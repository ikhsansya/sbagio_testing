<?php
/**
 * Created by PhpStorm.
 * User: donixo
 * Date: 11/1/18
 * Time: 11:33 PM
 */

namespace LaravelSbagio\Commands;


use Illuminate\Console\Command;
use Sbagio\Sbagio;

class Run extends Command
{
    protected $signature = 'sbagio:run';
    protected $description = 'mulai menjalankan proses sinkronisasi data kepegawaian';

    /**
     * @throws \Exception
     * @throws \Assert\AssertionFailedException
     */
    public function handle()
    {
        $this->info('menjalankan sbagio:run');

        $conf = require dirname(__FILE__) . '/../../configs/sbagio.php';
        $sbagio = Sbagio::createObject($conf['handlers']);
        $sbagio->setApiKey($conf['api']);
        $sbagio->sync();
    }
}