<?php

namespace LaravelSbagio\Components;

use App\Models\SbagioEvent;
use Sbagio\Entities\EventKepegawaian;
use Sbagio\Interfaces\Storage\LocalStorageInterface;
use Illuminate\Support\Facades\DB;

class StorageHandler implements LocalStorageInterface
{

    const TOKEN_KEY = 'token';
    const EXPIRATION_KEY = 'expired_on';
    const LOCK_KEY = 'lock';
    const LAST_ERR_KEY = 'last_err';

    const LOCK_ON = 'on';
    const LOCK_OFF = 'off';

    public function getLatestEventKepegawaianTimestamp()
    {
        $latest = SbagioEvent::max('created_at');

        if (!$latest) {
            return '2000-01-01';
        }

        return $latest;
    }

    public function exist(EventKepegawaian $event)
    {
        $found = SbagioEvent::where('hash', $event->hash())->first();

        if (!$found) {
            return false;
        }

        return true;
    }

    /**
     * @param $sql
     * @return array|bool
     */
    private function queryAll($sql)
    {
        if (!$query = DB::select($sql)) {
            return false;
        }

        return collect($query)->map(function($item){
            return collect($item)->toArray();
        });
    }

    /**
     * @param $sql
     * @return int
     */
    private function execute($sql)
    {
        return DB::statement($sql);
    }

    /**
     * @param EventKepegawaian $event
     * @return bool|void
     */
    public function addEventKepegawaian(EventKepegawaian $event)
    {
        if (!$this->exist($event)) {
            $sql = sprintf("INSERT INTO sbagio_events (hash, event_type, priority,
                    item_type, item_data, created_at)
                    VALUES('%s', '%s', %d, '%s', '%s', '%s');",
                $event->hash(),
                $event->eventType,
                $event->priority,
                $event->itemType,
                str_replace("'", "''", $event->itemData),
                $event->createdAt
            );

            $this->execute($sql);
        }
    }

    /**
     * @param string $key
     * @return string|null
     */
    private function getConfigValueOf($key)
    {
        $res = $this->queryAll(sprintf(
            "select conf_value from sbagio_configs where conf_key='%s' limit 1",
            $key
        ));

        if (empty($res)) {
            return null;
        }

        return $res[0]['conf_value'];
    }

    /**
     * @param $key
     * @param $val
     */
    private function addConfigOf($key, $val)
    {
        $this->execute(sprintf(
                "insert into sbagio_configs (conf_key, conf_value) values('%s', '%s')",
                $key, $val)
        );
    }

    /**
     * @param $key
     * @param $val
     * @return int
     */
    private function updateConfigOf($key, $val)
    {
        return $this->execute(sprintf(
                "update sbagio_configs set conf_value='%s' where conf_key='%s'",
                $val, $key)
        );
    }

    /**
     * @return null|string
     */
    public function token()
    {
        $qToken = $this->getConfigValueOf(self::TOKEN_KEY);

        if (empty($qToken)) {
            return null;
        }

        return $qToken;
    }

    /**
     * @param string $strToken
     * @param string $expiredOn
     * @return bool|void
     */
    public function updateToken($strToken, $expiredOn)
    {
        $token = $this->token();

        if (empty($token)) {
            $this->addConfigOf(self::TOKEN_KEY, $strToken);
            $this->addConfigOf(self::EXPIRATION_KEY, $expiredOn);
        } else {
            $this->updateConfigOf(self::TOKEN_KEY, $strToken);
            $this->updateConfigOf(self::EXPIRATION_KEY, $expiredOn);
        }
    }

    /**
     * @return bool
     */
    public function tokenHasExpired()
    {
        $expiration = $this->getConfigValueOf(self::EXPIRATION_KEY);

        if (!$expiration) {
            return true; //no token, force refresh
        }

        $expTstamp = strtotime($expiration);
        $now = strtotime('now');

        if ($now > $expTstamp) {
            return true;
        }

        return false;
    }

    public function lock()
    {
        $lockStatus = $this->getConfigValueOf(self::LOCK_KEY);

        if (!$lockStatus) {
            $this->addConfigOf(self::LOCK_KEY, self::LOCK_ON);
        } else {
            $this->updateConfigOf(self::LOCK_KEY, self::LOCK_ON);
        }
    }

    public function unlock()
    {
        $lockStatus = $this->getConfigValueOf(self::LOCK_KEY);

        if (empty($lockStatus)) {
            $this->addConfigOf(self::LOCK_KEY, self::LOCK_OFF);
        } else {
            $this->updateConfigOf(self::LOCK_KEY, self::LOCK_OFF);
        }

    }

    public function locked()
    {
        $lockStatus = $this->getConfigValueOf(self::LOCK_KEY);

        if (empty($lockStatus)) {
            return false;
        }

        if ($lockStatus == self::LOCK_ON) {
            return true;
        }

        return false;
    }


    public function markEventAsDone(EventKepegawaian $event)
    {
        /** @var SbagioEvent $found */
        $found = SbagioEvent::where('hash', $event->hash())->first();

        if (!$found) {
            return false;
        }

        $found->done_at = date('Y-m-d H:i:s');
        $found->save();

        return true;
    }

    public function fetchUnfinishedEvent()
    {
        $sql = "select * from sbagio_events where done_at is null
            order by priority, created_at, id asc";

        $result = $this->queryAll($sql);

        return collect($result)->map(function ($item) {
            $item['processed_at'] = $item['done_at'];
            unset($item['done_at']);
            return (new EventKepegawaian())->populate((object)$item);
        });
    }

    public function getlastErrorUpdaterPosition()
    {
        $lastErr = unserialize($this->getConfigValueOf(self::LAST_ERR_KEY));
        if (empty($lastErr)) {
            return null;
        }

        return $lastErr;
    }

    public function setLastErrorUpdaterPosition(int $since, int $page)
    {
        $lastErr = $this->getConfigValueOf(self::LAST_ERR_KEY);
        if (empty($lastErr)) {
            $this->addConfigOf(self::LAST_ERR_KEY, serialize([
                'since' => $since,
                'page' => $page
            ]));
        } else {
            $this->updateConfigOf(self::LAST_ERR_KEY, serialize([
                'since' => $since,
                'page' => $page
            ]));
        }
    }

    public function lastErrorPositionHandled()
    {
        $this->updateConfigOf(self::LAST_ERR_KEY, serialize(null));
    }
}
