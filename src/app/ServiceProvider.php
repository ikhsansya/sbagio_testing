<?php

namespace LaravelSbagio;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use LaravelSbagio\Commands\LumenPublish;
use LaravelSbagio\Commands\Run;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * @throws \Exception
     */
    public function boot()
    {
        if (!(file_exists(app_path('User.php')) || !file_exists('Models/User.php'))) {
            throw new \Exception('Silahkan menjalankan `artisan make:auth` terlebih dahulu');
        }

        $this->loadMigrations();

        $dir = dirname(__FILE__) . '/..';
        $copyMap = collect([]);

        if (!file_exists(app_path('Models/Organization.php'))) {
            $copyMap[$dir . '/models/Organization.php.txt'] = app_path('Models/Organization.php');
        }

        if (!file_exists(app_path('Models/Pegawai.php'))) {
            $copyMap[$dir . '/models/Pegawai.php.txt'] = app_path('Models/Pegawai.php');
        }

        if (!file_exists(app_path('Models/SbagioEvent.php'))) {
            $copyMap[$dir . '/models/SbagioEvent.php.txt'] = app_path('Models/SbagioEvent.php');
        }

        if (!file_exists(app_path('Models/KelompokSubstansi.php'))) {
            $copyMap[$dir . '/models/KelompokSubstansi.php.txt'] = app_path('Models/KelompokSubstansi.php');
        }

        if (!file_exists(app_path('Models/TimKerja.php'))) {
            $copyMap[$dir . '/models/TimKerja.php.txt'] = app_path('Models/TimKerja.php');
        }

        if (!file_exists(app_path('Models/TimKerjaMembership.php'))) {
            $copyMap[$dir . '/models/TimKerjaMembership.php.txt'] = app_path('Models/TimKerjaMembership.php');
        }

        if (!file_exists(app_path('Models/TimFokus.php'))) {
            $copyMap[$dir . '/models/TimFokus.php.txt'] = app_path('Models/TimFokus.php');
        }

        if (!file_exists(app_path('Models/TimFokusMembership.php'))) {
            $copyMap[$dir . '/models/TimFokusMembership.php.txt'] = app_path('Models/TimFokusMembership.php');
        }

        if ($copyMap->isNotEmpty()) {
            $this->publishes($copyMap->toArray());
        }

        if ($this->app->runningInConsole()) {
            $this->commands([
                Run::class,
                LumenPublish::class
            ]);
        }
    }

    public function register()
    {

    }

    public function loadMigrations()
    {
        $dir = dirname(__FILE__) . '/..';
        $copyMap = collect([]);

        $sbagioMigrationFile = glob(database_path('migrations/*create_sbagio_tables*'));
        if (count($sbagioMigrationFile) <= 0) {
            $timestamp = date('Y_m_d_His', time());
            $copyMap[$dir . '/migrations/create_sbagio_tables.php'] = database_path('migrations/' . $timestamp . '_create_sbagio_tables.php');
        }

        $kosubMigrationFile = glob(database_path('migrations/*create_kelompok_substansi_tables*'));
        if (count($kosubMigrationFile) <= 0) {
            $timestamp = date('Y_m_d_His', time() + 2);
            $copyMap[$dir . '/migrations/create_kelompok_substansi_tables.php'] = database_path('migrations/' . $timestamp . '_create_kelompok_substansi_tables.php');
        }

        $timKerjaMigrationFile = glob(database_path('migrations/*create_tim_kerja_tables*'));
        if (count($timKerjaMigrationFile) <= 0) {
            $timestamp = date('Y_m_d_His', time() + 2);
            $copyMap[$dir . '/migrations/create_tim_kerja_tables.php'] = database_path('migrations/' . $timestamp . '_create_tim_kerja_tables.php');
        }

        if ($copyMap->isNotEmpty()) {
            $this->publishes($copyMap->toArray());
        }
    }
}
