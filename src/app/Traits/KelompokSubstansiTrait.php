<?php


namespace LaravelSbagio\Traits;

use App\Models\KelompokSubstansi;

trait KelompokSubstansiTrait
{
    public function isPejabatKelompokSubstansi()
    {
        return !empty($this->pejabat_kelompok_substansi);
    }

    public function getPejabatKelompokSubstansi()
    {
        if (!$this->isPejabatKelompokSubstansi()) {
            return null;
        }

        return json_decode($this->pejabat_kelompok_substansi);
    }

    public function getKelompokSubstansi()
    {
        if (!$this->isPejabatKelompokSubstansi()) {
            return null;
        }

        return KelompokSubstansi::where('kode',
            $this->pejabat_kelompok_substansi->kode
        )->first();
    }

    public function setPejabatKelompokSubstansiToNull()
    {
        $this->pejabat_kelompok_substansi = null;
        $this->save();
    }
}
