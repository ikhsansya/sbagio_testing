<?php
/**
 * Created by PhpStorm.
 * User: donixo
 * Date: 11/1/18
 * Time: 8:17 PM
 */

use LaravelSbagio\Components\StorageHandler;

use LaravelSbagio\Consumers\Organisasi;
use LaravelSbagio\Consumers\Pegawai;
use LaravelSbagio\Consumers\RiwayatJabatan;
use LaravelSbagio\Consumers\RiwayatPelaksana;
use LaravelSbagio\Consumers\RiwayatPindahSatker;
use LaravelSbagio\Consumers\KelompokSubstansi;
use LaravelSbagio\Consumers\PegawaiKelompokSubstansi;
use LaravelSbagio\Consumers\TimKerja;
use LaravelSbagio\Consumers\TimKerjaMembership;
use LaravelSbagio\Consumers\TimFokus;
use LaravelSbagio\Consumers\TimFokusMembership;
use Sbagio\Interfaces\Consumer\OrganisasiConsumerInterface;
use Sbagio\Interfaces\Consumer\PegawaiConsumerInterface;
use Sbagio\Interfaces\Consumer\RiwayatJabatanConsumerInterface;
use Sbagio\Interfaces\Consumer\RiwayatPelaksanaConsumerInterface;
use Sbagio\Interfaces\Consumer\RiwayatPindahSatkerConsumerInterface;
use Sbagio\Interfaces\Consumer\KelompokSubstansiConsumerInterface;
use Sbagio\Interfaces\Consumer\PegawaiKelompokSubstansiConsumerInterface;
use Sbagio\Interfaces\Consumer\TimKerjaConsumerInterface;
use Sbagio\Interfaces\Consumer\TimKerjaMembershipConsumerInterface;
use Sbagio\Interfaces\Consumer\TimFokusConsumerInterface;
use Sbagio\Interfaces\Consumer\TimFokusMembershipConsumerInterface;
use Sbagio\Interfaces\Storage\LocalStorageInterface;

use Sbagio\Services\BrowserInterface;
use Sbagio\Services\ConsoleLogger;
use Sbagio\Services\GuzzleBrowser;
use Sbagio\Services\LoggerInterface;

return [
    'api' => [
        'url' => env('SBAGIO_API_HOST'),
        'username' => env('SBAGIO_USERNAME'),
        'password' => env('SBAGIO_PASSWORD')
    ],
    'handlers' => [
        LocalStorageInterface::class => StorageHandler::class,
        BrowserInterface::class => GuzzleBrowser::class,
        LoggerInterface::class => ConsoleLogger::class,
        PegawaiConsumerInterface::class => Pegawai::class,
        OrganisasiConsumerInterface::class => Organisasi::class,
        RiwayatJabatanConsumerInterface::class => RiwayatJabatan::class,
        RiwayatPelaksanaConsumerInterface::class => RiwayatPelaksana::class,
        RiwayatPindahSatkerConsumerInterface::class => RiwayatPindahSatker::class,
        KelompokSubstansiConsumerInterface::class => KelompokSubstansi::class,
        PegawaiKelompokSubstansiConsumerInterface::class => PegawaiKelompokSubstansi::class,
        TimKerjaConsumerInterface::class => TimKerja::class,
        TimKerjaMembershipConsumerInterface::class => TimKerjaMembership::class,
        TimFokusConsumerInterface::class => TimFokus::class,
        TimFokusMembershipConsumerInterface::class => TimFokusMembership::class
    ]
];
