<?php

if (!function_exists('app_path')) {
    function app_path($path = '') {
        return app()->basePath()
            . DIRECTORY_SEPARATOR . 'app'
            . ($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}


if (!function_exists('database_path')) {
    function database_path($path = '') {
        return app()->basePath()
            . DIRECTORY_SEPARATOR . 'database'
            . ($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}


if (!function_exists('event')) {
    function event($event, $payload = [], $halt = false) {
        return app('events')->dispatch($event, $payload, $halt);
    }
}