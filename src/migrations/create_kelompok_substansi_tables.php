<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;


class CreateKelompokSubstansiTables extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->jsonb('jabatan_kelompok_substansi')->nullable(); //note: if you use MariaDB, please change to longText type
        });

        Schema::create('kelompok_substansi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kode')->nullable();
            $table->integer('parent')->nullable();
            $table->string('nama')->nullable();
            $table->text('deskripsi')->nullable();
            $table->boolean('active')->nullable();
            $table->string('parent_kode_unit_kerja')->nullable();
            $table->string('path')->nullable();
            $table->timestamps();
        });

    }

    public function down()
    {

    }
}
