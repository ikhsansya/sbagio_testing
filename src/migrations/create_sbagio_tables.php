<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Created by PhpStorm.
 * User: donixo
 * Date: 11/1/18
 * Time: 11:30 PM
 */

class CreateSbagioTables extends Migration
{
    public function up()
    {
        Schema::create('sbagio_configs', function (Blueprint $table) {
            $table->id();
            $table->string('conf_key', 255)->nullable();
            $table->string('conf_value', 1024)->nullable();
        });

        Schema::create('sbagio_events', function (Blueprint $table) {
            $table->id();
            $table->string('hash', 64)->nullable();
            $table->string('event_type', 128);
            $table->integer('priority')->default(0);
            $table->string('item_type', 128);
            $table->jsonb('item_data'); //note: if you use MariaDB, please change to longText type
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('done_at')->nullable();
        });

        // Schema::table('users', function (Blueprint $table) {
        //     $table->string('username')->nullable();
        //     $table->smallInteger('active')->nullable()->default(1);
        //     $table->jsonb('organisasi')->nullable(); //note: if you use MariaDB, please change to longText type
        //     $table->string('nip')->nullable();
        //     $table->string('pangkat_golongan')->nullable();
        //     $table->jsonb('jabatan_struktural_organisasi')->nullable(); //note: if you use MariaDB, please change to longText type
        //     $table->jsonb('plt_jabatan_struktural_organisasi')->nullable(); //note: if you use MariaDB, please change to longText type
        //     $table->jsonb('plh_jabatan_struktural_organisasi')->nullable(); //note: if you use MariaDB, please change to longText type
        //     $table->string('nama_jabatan_fungsional_umum')->nullable();
        //     $table->string('nama_jabatan_fungsional_tertentu')->nullable();
        //     $table->string('profile_picture')->nullable();
        //     $table->dropUnique('users_email_unique');
        // });

        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->nullable();
            $table->string('path')->nullable();
            $table->string('kode_simpatik')->nullable();
            $table->string('parent_kode_simpatik')->nullable();
            $table->string('nama_organisasi')->nullable();
            $table->string('nama_kota')->nullable();
            $table->string('nama_jabatan')->nullable();
            $table->boolean('active')->nullable();
            $table->timestamps();
        });

    }

    public function down()
    {

    }
}
