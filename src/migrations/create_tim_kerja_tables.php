<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;


class CreateTimKerjaTables extends Migration
{
    public function up()
    {
        //table tim kerja
        Schema::create('tim_kerja', function (Blueprint $table) {
            $table->id();
            $table->integer('id_tim_kerja');
            $table->string('kode_satker', 255)->nullable();
            $table->string('name', 255)->nullable();
            $table->string('status', 255)->nullable();
            $table->timestamp('activated_at')->nullable();
            $table->timestamp('deactivated_at')->nullable();
            $table->timestamp('rejected_at')->nullable();
            $table->text('rejected_reason')->nullable();
        });

        //table tim kerja membership
        Schema::create('tim_kerja_membership', function (Blueprint $table) {
            $table->id();
            $table->integer('id_tim_kerja_membership');
            $table->string('nip', 255)->nullable();
            $table->integer('id_tim_kerja')->nullable();
            $table->string('type', 255)->nullable();
            $table->string('role', 255)->nullable();
            $table->string('status', 255)->nullable();
            $table->timestamp('activated_at')->nullable();
            $table->timestamp('deactivated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });

        //table tim fokus
        Schema::create('tim_fokus', function (Blueprint $table) {
            $table->id();
            $table->integer('id_tim_fokus');
            $table->integer('id_tim_kerja')->nullable();
            $table->string('name', 255)->nullable();
            $table->string('status', 255)->nullable();
            $table->timestamp('activated_at')->nullable();
            $table->timestamp('deactivated_at')->nullable();
        });

        //table tim fokus membership
        Schema::create('tim_fokus_membership', function (Blueprint $table) {
            $table->id();
            $table->integer('id_tim_fokus_membership');
            $table->string('nip', 255)->nullable();
            $table->integer('id_tim_fokus')->nullable();
            $table->string('type', 255)->nullable();
            $table->string('role', 255)->nullable();
            $table->string('status', 255)->nullable();
            $table->timestamp('activated_at')->nullable();
            $table->timestamp('deactivated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });

    }

    public function down()
    {
        //
    }
}
