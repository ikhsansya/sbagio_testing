# Laravel Sbagio


## Introduction

Laravel Sbagio will help you to bootstrap base kepegawaian data from simpatik into a new application based on laravel and lumen

## Requirements

At this time, Laravel Sbagio support:
- Postgresql >= 10 database.
- MariaDB

Use the following table as guideline to determine `laravel-sbagio` version based on PHP/Laravel version of your application:

| `laravel-sbagio` | `sbagio`           | PHP           | Lumen / Laravel |
|------------------|--------------------|---------------|-----------------|
| 1.x              | 1.x / dev-master   | >=7.1.0       | 5.3 - 5.7       |
| 2.x              | 1.x / dev-master   | >=7.2         | ^7.0            |
| 3.x              | 2.0                | >=7.3 / >=8.0 | ^8.0            |
| 4.x              | 3.0                | >8.0          | ^9.0            |

## Installation

First, add gitlab kominfo repo definition inside `composer.json`:


```
"repositories": [
	{
	  "type": "vcs",
	  "url": "https://gitlab.dev.kominfo.go.id/donixo/laravel-sbagio"
	},
	{
	  "type": "vcs",
	  "url": "https://gitlab.dev.kominfo.go.id/donixo/sbagio"
	}
]
```

then run `composer require donixo/laravel-sbagio`.


### Laravel


Please bear in mind that laravel sbagio will assume that application is in fresh setup state after you had just 
finished running `artisan make:auth` with no further modification, so there's no organizations table and users table only has minimum columns from `make:auth`

To publish organizations table and package config into application, run:

```
$ php artisan vendor:publish --provider="LaravelSbagio\ServiceProvider"
```

above command will create `app/Models/Organizations.php` file and also will create a new migration file with following sql tables and columns related to pegawai/organisasi.

> note: if you use MariaDB, please change `jsonb` to `longText` type on migration files

to execute newly created migrations, run:

```bash
$ php artisan migrate
```


next, we should move `app/User.php` file into `app/Models/User.php` and do not forget to change namespace reference inside `User.php` into `App\Models`.

also, other than above descriptions, Laravel Sbagio will also 
create table `sbagio_events` and `sbagio_configs` to handle sbagio event 
records and token handling

### Lumen

with default installation, lumen doesn't have any of `make:auth` command, so we need to create user 
table migration and user model files.


To do that, replace your newly created file from `make:migration` command:

```php
Schema::create('users', function (Blueprint $table) {
    $table->bigIncrements('id');
    $table->string('name');
    $table->string('email')->unique();
    $table->timestamp('email_verified_at')->nullable();
    $table->string('password');
    $table->rememberToken();
    $table->timestamps();
});
```

after that, create new `App/Models/User.php` or if you have file `App/User.php`, please move into `App/Models/User.php`:


```php
<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}

```

next, we need to add laravel-sbagio service provider into `bootstrap/app.php`:

```php
<?php
//...
$app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\AuthServiceProvider::class);
$app->register(LaravelSbagio\ServiceProvider::class);
$app->register(\App\Providers\EventServiceProvider::class);
```

when done, run `php artisan sbagio:lumen-publish && php artisan migrate` to put things into effects.

finally, for both laravel & lumen add following parameters into `.env`:

```
SBAGIO_API_HOST=https://your-simpatik-api-host
SBAGIO_USERNAME=api-user
SBAGIO_PASSWORD=some-api-password
```


List of emitted events from `laravel-sbagio` are listed below:


### Users

<table>
	<thead>
		<tr>
			<td>Column</td>
			<td>Data Type</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>username</td>
			<td>string</td>
		</tr>
		<tr>
			<td>active</td>
			<td>small integer</td>
		</tr>
		<tr>
			<td>organisasi</td>
			<td>jsonb</td>
		</tr>
		<tr>
			<td>nip</td>
			<td>string</td>
		</tr>
		<tr>
			<td>pangkat_golongan</td>
			<td>string</td>
		</tr>
		<tr>
			<td>jabatan_struktural_organisasi</td>
			<td>jsonb</td>
		</tr>
		<tr>
			<td>plt_jabatan_struktural_organisasi</td>
			<td>jsonb</td>
		</tr>
		<tr>
			<td>plh_jabatan_struktural_organisasi</td>
			<td>jsonb</td>
		</tr>
		<tr>
			<td>nama_jabatan_fungsional_umum</td>
			<td>string</td>
		</tr>
		<tr>
			<td>nama_jabatan_fungsional_tertentu</td>
			<td>string</td>
		</tr>
		<tr>
			<td>profile_picture</td>
			<td>string</td>
		</tr>
		<tr>
            <td>jabatan_kelompok_substansi</td>
            <td>string</td>
        </tr>
	</tbody>
</table>



### Organizations

<table>
	<thead>
		<tr>
			<td>Column</td>
			<td>Data Type</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>id</td>
			<td>serial</td>
		</tr>
		<tr>
			<td>parent_id</td>
			<td>integer</td>
		</tr>
		<tr>
			<td>path</td>
			<td>string</td>
		</tr>
		<tr>
			<td>kode_simpatik</td>
			<td>string</td>
		</tr>
		<tr>
			<td>parent_kode_simpatik</td>
			<td>string</td>
		</tr>
		<tr>
			<td>nama_organisasi</td>
			<td>string</td>
		</tr>
		<tr>
			<td>nama_kota</td>
			<td>string</td>
		</tr>
		<tr>
			<td>nama_jabatan</td>
			<td>string</td>
		</tr>
		<tr>
			<td>active</td>
			<td>boolean</td>
		</tr>
    </tbody>
</table>

## Kelompok Substansi
<table>
	<thead>
		<tr>
			<td>Column</td>
			<td>Data Type</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>id</td>
			<td>serial</td>
		</tr>
        <tr>
            <td>kode</td>
            <td>integer</td>
        </tr>
		<tr>
			<td>parent</td>
			<td>integer</td>
		</tr>
		<tr>
			<td>nama</td>
			<td>string</td>
		</tr>
		<tr>
			<td>deskripsi</td>
			<td>text</td>
		</tr>
		<tr>
			<td>active</td>
			<td>boolean</td>
		</tr>
		<tr>
			<td>parent_kode_unit_kerja</td>
			<td>integer</td>
		</tr>
		<tr>
			<td>path</td>
			<td>string</td>
		</tr>
    </tbody>
</table>

## Usage

To run laravel sbagio command, you can use artisan:

```
$ php artisan sbagio:run
```

## Events

To listen events after sbagio run, we can listen to following events in `EventServiceProvider`:

### Organization

<table>
	<tbody>
		<tr>
			<td>organization.created</td>
		</tr>
		<tr>
			<td>organization.updated</td>
		</tr>
		<tr>
			<td>organization.activated</td>
		</tr>
		<tr>
			<td>organization.deactivated</td>
		</tr>
	</tbody>
</table>

### Kelompok Substansi

<table>
	<tbody>
		<tr>
			<td>kelompok-substansi.created</td>
		</tr>
		<tr>
			<td>kelompok-substansi.updated</td>
		</tr>
		<tr>
			<td>kelompok-substansi.activated</td>
		</tr>
		<tr>
			<td>kelompok-substansi.deactivated</td>
		</tr>
	</tbody>
</table>

### Pegawai Kelompok Substansi

<table>
    <tbody>
		<tr>
			<td>pegawai-kelompok-substansi.created</td>
		</tr>
		<tr>
			<td>pegawai-kelompok-substansi.updated</td>
		</tr>
		<tr>
			<td>pegawai-kelompok-substansi.removed</td>
		</tr>
    </tbody>
</table>

### Pegawai

<table>
    <tbody>
		<tr>
			<td>pegawai.created</td>
		</tr>
		<tr>
			<td>pegawai.updated</td>
		</tr>
		<tr>
			<td>pegawai.photo-uploaded</td>
		</tr>
		<tr>
			<td>pegawai.photo-removed</td>
		</tr>
		<tr>
			<td>pegawai.username-changed</td>
		</tr>
		<tr>
			<td>pegawai.jabatan-updated</td>
		</tr>
		<tr>
			<td>pegawai.status-updated</td>
		</tr>
		<tr>
			<td>pegawai.activated</td>
		</tr>
		<tr>
			<td>pegawai.deactivated</td>
		</tr>
    </tbody>
</table>

### Riwayat Jabatan

<table>
    <tbody>
		<tr>
			<td>jabatan.created</td>
		</tr>
		<tr>
			<td>jabatan.updated</td>
		</tr>
		<tr>
			<td>jabatan.removed</td>
		</tr>
    </tbody>
</table>

### Riwayat Pelaksana

<table>
    <tbody>
		<tr>
			<td>pelaksana.created</td>
		</tr>
		<tr>
			<td>pelaksana.updated</td>
		</tr>
		<tr>
			<td>pelaksana.removed</td>
		</tr>
    </tbody>
</table>

### Riwayat Pindah Satker

<table>
    <tbody>
		<tr>
			<td>pindah-satker.created</td>
		</tr>
		<tr>
			<td>pindah-satker.updated</td>
		</tr>
		<tr>
			<td>pindah-satker.removed</td>
		</tr>
    </tbody>
</table>

### Tim Kerja

<table>
    <tbody>
		<tr>
			<td>tim-kerja.created</td>
		</tr>
		<tr>
			<td>tim-kerja.updated</td>
		</tr>
		<tr>
			<td>tim-kerja.activated</td>
		</tr>
		<tr>
			<td>tim-kerja.deactivated</td>
		</tr>
    </tbody>
</table>

### Tim Kerja Membership

<table>
    <tbody>
		<tr>
			<td>tim-kerja-membership.added</td>
		</tr>
		<tr>
			<td>tim-kerja-membership.updated</td>
		</tr>
		<tr>
			<td>tim-kerja-membership.deactivated</td>
		</tr>
    </tbody>
</table>

### Tim Fokus

<table>
    <tbody>
		<tr>
			<td>tim-fokus.created</td>
		</tr>
		<tr>
			<td>tim-fokus.updated</td>
		</tr>
		<tr>
			<td>tim-fokus.activated</td>
		</tr>
		<tr>
			<td>tim-fokus.deactivated</td>
		</tr>
    </tbody>
</table>

### Tim Fokus Membership

<table>
    <tbody>
		<tr>
			<td>tim-fokus-membership.added</td>
		</tr>
		<tr>
			<td>tim-fokus-membership.updated</td>
		</tr>
		<tr>
			<td>tim-fokus-membership.deactivated</td>
		</tr>
    </tbody>
</table>

sample code:

```php
class EventServiceProvider extends ServiceProvider
{
   //...
   
    public function boot()
    {
        parent::boot();

        Event::listen('organization.*', function ($eventName, $data) {
            assign to inbox
            remove role dst...
        });
        
         Event::listen('organization.created', function ($eventName, $data) {
            assign to inbox
            remove role dst...
        });
    }
}
```

